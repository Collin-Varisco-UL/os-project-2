package com.company;
import java.util.Random;

public class User implements Runnable{
    int requests= 5;
    int domain;
    int domainsNum;
    int objectsNum;
    int threadId;

    AccessRights accessRights;

    public User(int threadId, int domain, int domains, int objects, AccessRights accessRights) {
        this.threadId = threadId;
        this.accessRights = accessRights;
        this.domain = domain;
        this.domainsNum = domains;
        this.objectsNum = objects;
    }

    @Override
    public void run() {
        Random randomGen = new Random();
        int randomObject;
        int randomAction;

        /** Kelsey Ferguson
         * The for loop lasts until all requests have been made
         * */
        for (int i = 0; i < requests; i++) {

            /** Kelsey Ferguson
             * Random object is the object that will be read from/written to/switched to. Random Action is either
             * 0 (read) or 1 (write).
             */
            randomObject = randomGen.nextInt(1, (domainsNum + objectsNum));

            /** Kelsey Ferguson
             * Not allowing a user in a domain to switch to the domain they're already in
             * */
            while (randomObject > objectsNum && domain == ((objectsNum + domainsNum) - randomObject))
                randomObject = randomGen.nextInt(1, (domainsNum + objectsNum));

            randomAction = randomGen.nextInt(2);

            /** Kelsey Ferguson
             * If the random object number generated is greater than the number of objects, it must be a domain.
             * According to the project requirements, the domain that that should be attempted to be switched to is
             * the domain with the number (N (domains) + M (objects)) - X (randomly generated number).
             *
             */
            if (randomObject > objectsNum) {
                randomObject = (objectsNum + domainsNum) - randomObject;
                System.out.printf("[Thread: %s(D%s)] Attempting to switch from D%s to D%s. %n", threadId, domain, domain, randomObject);

                if (this.accessRights.Switch(randomObject, domain)) {
                    System.out.printf("[Thread: %s(D%s)] Switched to D%s %n", threadId, randomObject, randomObject);
                    this.domain = randomObject;

                    int yieldNum = randomGen.nextInt(3, 8);
                    System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                    for (int n = 0; n < yieldNum; n++)
                        Thread.yield();
                    System.out.printf("[Thread: %s(D%s)] Operation Complete. %n", threadId, domain);
                }
                else {
                    System.out.printf("[Thread: %s(D%s)] Operation Failed, Permission Denied. %n", threadId, domain);
                    int yieldNum = randomGen.nextInt(3, 8);
                    System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                    for (int n = 0; n < yieldNum; n++)
                        Thread.yield();
                }
            }

            /** Kelsey Ferguson
             * This is the code for either reading or writing to an object
             */
            else {
                if (randomAction == 0)
                    this.accessRights.Read(threadId, randomObject, domain);
                else
                    this.accessRights.Write(threadId, randomObject, domain);
            }
        }
    }
}
