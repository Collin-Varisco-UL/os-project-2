package com.company;

import java.util.Hashtable;
import java.util.Random;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.Map;

public class AccessRights {
    int taskNumber;
    int objectNum;
    int domainNum;

    String[][] accessMatrix; // Kelsey Ferguson: This is the matrix used for the first task.
    ArrayList<String>[] accessList; // Collin Varisco: The ArrayLists in the array are lists of access rights for an object or domain.
    Hashtable<String,Hashtable<String,String>> capabilityList = new Hashtable<String,Hashtable<String,String>>(); // Grant Askin: Nested Hashtables contain access rights for a specific domain.

    /** Kelsey Ferguson
     * This array represents the contents of the objects. Below it are the random words used to populate this array.
     * Those phrases will also be used when writing to an object.
     */
    static String[] objects;

    String[] phrasesForObjects = {
            "Red",
            "Purple",
            "Blue",
            "Orange",
            "Grey",
            "White",
            "Green",
            "Brown",
            "Black",
            "Yellow",
            "Apple",
            "Orange",
            "Strawberry",
            "Blueberry",
            "Banana",
            "Mango",
            "Grape",
            "Raspberry"
    };

    static Semaphore[] objectsAccess;

    String[] rights = {
            "R",
            "W",
            "R/W",
            " "
    };

    Random randomGen = new Random();

    public AccessRights(int taskNumber, int domainsNum, int objectsNum) {
        this.taskNumber = taskNumber;
        this.objectNum = objectsNum;
        this.domainNum = domainsNum;


        objects = new String[objectsNum];
        objectsAccess = new Semaphore[objectsNum];
        for (int i = 0; i < objectsNum; i++) {
            objects[i] = phrasesForObjects[(randomGen.nextInt(domainsNum))];
            objectsAccess[i] = new Semaphore(1);
        }

        // Initialize Access List ArrayList
        this.accessList = new ArrayList[domainsNum + objectsNum];

        switch(taskNumber) {
            case 1:
                populateMatrix();
                printMatrix();
                break;
            case 2:
                populateAccessList();
                printAccessList();
                break;
            case 3:
                populateCapabilityList();
                printCapabilityList();
                break;
        }
    }

    /**
     * Kelsey Ferguson
     * This function is used for populating the matrix with access rights.
     * The very first row of the matrix is the list of objects and the very first column is the list of domains.
     * */
    public void populateMatrix () {
        this.accessMatrix = new String[domainNum+1][(objectNum + domainNum + 1)];
        this.accessMatrix[0][0] = "Domain/Object";

        /** Kelsey Ferguson
         * Filling in that first column
         */
        for (int i = 1; i <= domainNum; i++) {
            this.accessMatrix[i][0] = "D" + i;
        }

        /** Kelsey Ferguson
         * Filling in that first row
         */
        for (int i = 1; i <= (domainNum + objectNum); i++) {
            if (i > objectNum)
                this.accessMatrix[0][i] = "D" + (i - objectNum);
            else
                this.accessMatrix[0][i] = "F" + i;
        }

        /** Kelsey Ferguson
         * Filling in the rest of the matrix. Objects should have read and write permissions while domains should
         * have it specified whether they can be switched to. If the row number is equal to n - objectNum,
         * this is the same domain meaning it'll be an empty entry.
         */
        for (int i = 1; i <= domainNum; i++)
            for (int n = 1; n <= (domainNum + objectNum); n++) {
                if (n > objectNum) {
                    if (i == (n - objectNum))
                        this.accessMatrix[i][n] = " ";
                    else {
                        if (randomGen.nextInt(2) == 1)
                            this.accessMatrix[i][n] = "Allow";
                        else
                            this.accessMatrix[i][n] = " ";
                    }
                }
                else {
                    accessMatrix[i][n] = rights[(randomGen.nextInt(4))];
                }
            }
    }

    /** Kelsey Ferguson
     * Outputting the matrix
     */
    public void printMatrix() {
        System.out.println("Access Control Scheme: Access Matrix");
        System.out.printf("Domain Count: %s%n", domainNum);
        System.out.printf("Object Count: %s%n", objectNum);

        for (int i = 0; i < accessMatrix.length; i++) {
            if (i > 0)
                System.out.print("     ");
            for (int n = 0; n < accessMatrix[i].length; n++) {
                System.out.printf("%10s", ("  " + accessMatrix[i][n]));
            }
            System.out.println();
        }
    }

    /** Collin Varisco
     * This function will populate the access list
     */
    public void populateAccessList() {
        for (int i = 1; i <= domainNum + objectNum; i++)
            this.accessList[i - 1] = new ArrayList<>();

        // Specify the first element of each list as an object
        for (int i = 0; i < objectNum; i++)
            this.accessList[i].add("F" + (i + 1));

        // Specify the first element of each list as a domain
        for (int i = 1; i <= (domainNum); i++)
            this.accessList[i+objectNum-1].add("D" + (i));

        // Fill in each list with the appropriate rights
        for (int i = 1; i <= (domainNum + objectNum); i++) {
            for (int n = 1; n <= domainNum; n++) {
                if ((i-1) < objectNum && randomGen.nextBoolean())
                    this.accessList[i-1].add("D" + n + ":" + rights[(randomGen.nextInt(3))]);
                if ((i-1) >= objectNum && n != (i-objectNum) && randomGen.nextBoolean())
                    this.accessList[i-1].add("D" + n + ":Allow");
            }
        }
    }

    /** Collin Varisco
     * This function will print the access list
     */
    public void printAccessList() {
        System.out.println("Access Control Scheme: Access List");
        System.out.printf("Domain Count: %s%n", domainNum);
        System.out.printf("Object Count: %s%n", objectNum);

        for(int i = 0; i < (objectNum+domainNum); i++) {
            System.out.print(this.accessList[i].get(0) + " --> ");
            for (int j = 1; j < this.accessList[i].size(); j++) {
                System.out.print(this.accessList[i].get(j) + " ");
            }
            System.out.println();
        }
    }

    /** Kelsey Ferguson
     * Going off of the task number, this read function redirects to another read function for the respective task.
     * The write and switch functions do the same.
     * */
    public void Read(int threadId, int object, int domain) {
        if (taskNumber == 1)
            Task1Read(threadId, object, domain);
        else if(taskNumber == 2)
            Task2Read(threadId, object, domain);
        else if(taskNumber == 3)
            Task3Read(threadId, object, domain);
    }
    public void Write(int threadId, int object, int domain) {
        if (taskNumber == 1)
            Task1Write(threadId, object, domain);
        else if(taskNumber == 2)
            Task2Write(threadId, object, domain);
        else if(taskNumber == 3)
            Task3Write(threadId, object, domain);
    }
    public Boolean Switch(int object, int domain) {
        if (taskNumber == 1)
            return Task1Switch(object, domain);
        else if(taskNumber == 2)
            return Task2Switch(object, domain);
        else if(taskNumber == 3)
            return Task3Switch(object, domain);
        else
            return false;
    }

    /** Kelsey Ferguson
     * The appropriate entry of the matrix is checked and if it contains "R" or "R/W", the contents can be read.
     * If the object can be read from, that occurs in this function. The write function is very similar.
     */
    public void Task1Read(int threadId, int object, int domain) {

        try {
            System.out.printf("[Thread: %s(D%s)] Attempting to read resource: F%s %n", threadId, domain, object);
            if (accessMatrix[domain][object].equals("R") || accessMatrix[domain][object].equals("R/W")) {
                objectsAccess[object-1].acquire();
                Thread.yield();

                System.out.printf("[Thread: %s(D%s)] Resource F%s contains '%s' %n", threadId, domain, object,
                        objects[object - 1]);

                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
                System.out.printf("[Thread: %s(D%s)] Operation Complete. %n", threadId, domain);

                objectsAccess[object-1].release();

            }
            else {
                System.out.printf("[Thread: %s(D%s)] Operation Failed, Permission Denied %n", threadId, domain);
                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /** Kelsey Ferguson
     * The index for objects is given as object - 1 because the object id count starts at 1 but the array starts at
     * 0. So the index for an object is its number id - 1.
     */
    public void Task1Write(int threadId, int object, int domain) {

        try {
            System.out.printf("[Thread: %s(D%s)] Attempting to write to resource: F%s %n", threadId, domain, object);
            String change;
            if (accessMatrix[domain][object].equals("W") || accessMatrix[domain][object].equals("R/W")) {
                objectsAccess[object-1].acquire();
                Thread.yield();

                change = phrasesForObjects[randomGen.nextInt(phrasesForObjects.length)];
                objects[object - 1] = change;
                System.out.printf("[Thread: %s(D%s)] Writing '%s' to resource F%s %n", threadId, domain, change, object);

                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
                System.out.printf("[Thread: %s(D%s)] Operation Complete. %n", threadId, domain);

                objectsAccess[object-1].release();
            } else {
                System.out.printf("[Thread: %s(D%s)] Operation Failed, Permission Denied %n", threadId, domain);
                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
            }

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /** Kelsey Ferguson
     * Whether a domain can be switched to is returned as a boolean value to the thread. If it can switch,
     * that occurs in the run method of the User class.
     */
    public Boolean Task1Switch(int object, int domain) {
        return (accessMatrix[domain][object+objectNum].equals("Allow"));
    }

    /** Collin Varisco
     * This function will check the access list to see if a domain can write to an object.
     * If the domain has permission to write, the object's phrase variable will be written to.
     */
    public void Task2Write(int threadId, int object, int domain){
        try {
            System.out.printf("[Thread: %s(D%s)] Attempting to write to resource: F%s %n", threadId, domain, object);
            if (accessList[object - 1].contains("D" + domain + ":W") || accessList[object - 1].contains("D" + domain + ":R/W")) {
                objectsAccess[object - 1].acquire();
                Thread.yield();

                String changedPhrase = phrasesForObjects[randomGen.nextInt(phrasesForObjects.length)];
                objects[object - 1] = changedPhrase;
                System.out.printf("[Thread: %s(D%s)] Writing '%s' to resource F%s %n", threadId, domain, changedPhrase, object);

                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
                System.out.printf("[Thread: %s(D%s)] Operation Complete. %n", threadId, domain);

                objectsAccess[object - 1].release();
            } else {
                System.out.printf("[Thread: %s(D%s)] Operation Failed, Permission Denied %n", threadId, domain);
                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

     /** Collin Varisco
     * A boolean value is returned that represents whether domain can switch to another domain.
     */
    public boolean Task2Switch(int object, int domain) {
        return accessList[object + objectNum - 1].contains("D" + domain + ":Allow");
    }

    /** Collin Varisco
     * This function will check the access list to see if a domain can read from an object.
     * If the domain has access, the object's phrase variable will be read from.
     */
    public void Task2Read(int threadId, int object, int domain){
        try {
            System.out.printf("[Thread: %s(D%s)] Attempting to read resource: F%s %n", threadId, domain, object);
            if (accessList[object - 1].contains("D" + domain + ":R") || accessList[object - 1].contains("D" + domain + ":R/W")) {
                objectsAccess[object - 1].acquire();
                Thread.yield();

                System.out.printf("[Thread: %s(D%s)] Resource F%s contains '%s' %n", threadId, domain, object, objects[object-1]);

                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
                System.out.printf("[Thread: %s(D%s)] Operation Complete. %n", threadId, domain);

                objectsAccess[object - 1].release();
            } else {
                System.out.printf("[Thread: %s(D%s)] Operation Failed, Permission Denied %n", threadId, domain);
                int yieldNum = randomGen.nextInt(3, 8);
                System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
                for (int n = 0; n < yieldNum; n++)
                    Thread.yield();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    /** Grant Askin
    * This function will populate the hashmaps with appropriate keys and values.
    */
    public void populateCapabilityList() {
        for (int i = 0; i < domainNum; i++ ) {
            // Create index for domain
            capabilityList.put("D" + (i+1), new Hashtable<String,String>());
            Hashtable<String,String> domainTable = capabilityList.get("D" + (i+1));
            // Populate each object's rights
            for (int j = 0; j < objectNum; j++) {
                String right = rights[randomGen.nextInt(rights.length)];
                if (right == " ") continue;
                else {
                    domainTable.put("F" + (j+1), right);
                }
            }
            // Populate each domain's switching rights
            for (int k = 0; k < domainNum; k++) {
                if (k == i) continue;
                String right = (randomGen.nextInt(2) == 1) ? "Allow" : " ";
                if (right == " ") continue;
                else {
                    domainTable.put("D" + (k+1), right);
                }
            }
        }
    }
    /** Grant Askin
    * This function will print out the capability list.
    */
    public void printCapabilityList() {
        System.out.println("Access Control Scheme: Capability List");
        System.out.printf("Domain Count: %s%n", domainNum);
        System.out.printf("Object Count: %s%n", objectNum);

        for (int i = 0; i < domainNum; i++ ) {
            System.out.printf("D%d -> ", i+1);
            for (Map.Entry<String,String> entry : capabilityList.get("D" + (i+1)).entrySet()) {
                System.out.printf("%s:%s ", entry.getKey(), entry.getValue());
            }
            System.out.println();
        }
    }
    /** Grant Askin
    * This function will check the access list to see if a domain can read an object.
    */
    public void Task3Read(int threadId, int object, int domain) {
        String right = capabilityList.get("D" + domain).get("F" + object);
        System.out.printf("[Thread: %s(D%s)] Attempting to read resource: F%s %n", threadId, domain, object);
        if (right == "R" || right == "R/W") {
            objectsAccess[object-1].acquireUninterruptibly();
            Thread.yield();

            System.out.printf("[Thread: %s(D%s)] Resource F%s contains '%s' %n", threadId, domain, object, objects[object-1]);

            int yieldNum = randomGen.nextInt(3, 8);
            System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
            for (int n = 0; n < yieldNum; n++)
                Thread.yield();
            System.out.printf("[Thread: %s(D%s)] Operation Complete. %n", threadId, domain);

            objectsAccess[object-1].release();
        }
        else {
            System.out.printf("[Thread: %s(D%s)] Operation Failed, Permission Denied %n", threadId, domain);
            int yieldNum = randomGen.nextInt(3, 8);
            System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
            for (int n = 0; n < yieldNum; n++)
                Thread.yield();
        }
    }
    /** Grant Askin
    * This function will check the access list to see if a domain can write to an object.
    */
    public void Task3Write(int threadId, int object, int domain) {
        String right = capabilityList.get("D" + domain).get("F" + object);
        System.out.printf("[Thread: %s(D%s)] Attempting to write to resource: F%s %n", threadId, domain, object);
        if (right == "W" || right == "R/W") {
            String change = phrasesForObjects[randomGen.nextInt(phrasesForObjects.length)];
            objectsAccess[object-1].acquireUninterruptibly();
            Thread.yield();

            objects[object - 1] = change;
            System.out.printf("[Thread: %s(D%s)] Writing '%s' to resource F%s %n", threadId, domain, change, object);

            int yieldNum = randomGen.nextInt(3, 8);
            System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
            for (int n = 0; n < yieldNum; n++)
                Thread.yield();
            System.out.printf("[Thread: %s(D%s)] Operation Complete. %n", threadId, domain);

            objectsAccess[object-1].release();
        }
        else {
            System.out.printf("[Thread: %s(D%s)] Operation Failed, Permission Denied %n", threadId, domain);
            int yieldNum = randomGen.nextInt(3, 8);
            System.out.printf("[Thread: %s(D%s)] Yielding %s times. %n", threadId, domain, yieldNum);
            for (int n = 0; n < yieldNum; n++)
                Thread.yield();
        }
    }
    /** Grant Askin
    * This function will check the access list to see if a user can switch to another domain.
    */
    public boolean Task3Switch(int target, int domain) {
        return (capabilityList.get("D" + domain).get("D" + target) == "Allow");
    }

}
