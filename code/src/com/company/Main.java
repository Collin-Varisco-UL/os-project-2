package com.company;
import java.util.Random;

public class Main {
    /* Collin Varisco
     * Task 4 - Handle Command Line Arguments
     * args - arguments passed from the command line
     * Processes the command line argument for which task to run, and then starts the respective task
     * with the startTask(int taskNumber) method.
     */
    static void handleCommandLineArguments(String[] args) {
        // Exits if the first argument is invalid
        if (!args[0].equals("-rs") && args.length < 2 || (args[0].equals("-rs") && args.length < 4) ||
                (!args[0].equals("-rs") && args.length > 2) || (args[0].equals("-rs") && args.length > 4)) {
                System.out.println("Invalid first argument");
                System.exit(0);
        } else {
            try {
                int taskNumber = Integer.parseInt(args[1]);
                if (args[0].equals("-S")) {
                    if (taskNumber > 0 && taskNumber < 4) {
                        startTask(taskNumber, false, 0, 0);
                    } else {
                        System.out.println("Invalid task number");
                        System.exit(0);
                    }
                } else if(args[0].equals("-rs")) {
                    if (taskNumber > 0 && taskNumber < 4) {
                        int domains = Integer.parseInt(args[2]);
                        int objects = Integer.parseInt(args[3]);
                        if(domains > 0 && objects > 0) {
                            startTask(taskNumber, true, domains, objects);
                        } else {
                            System.out.println("Invalid number of domains or objects");
                            System.exit(0);
                        }
                    } else {
                        System.out.println("Invalid task number");
                        System.exit(0);
                    }
                } else {
                    System.out.println("Invalid first argument");
                    System.exit(0);
                }
            } catch (Exception e) {
                System.out.println("A string was passed where an integer was expected");
                System.exit(0);
            }
        }
    }

    /**
     * Collin Varisco
     * startTask(int taskNumber)
     * taskNumber - the number of the task to run
     * Starts the task by passing the taskNumber parameter to the AccessRights constructor.
     */
    public static void startTask(int taskNumber, boolean commandLine, int domains, int objects) {
        int domainsNum = (!commandLine) ? new Random().nextInt(3, 8) : domains;
        int objectsNum = (!commandLine) ? new Random().nextInt(3, 8) : objects;

        Thread[] userThreads = new Thread[domainsNum];

        AccessRights accessRights = new AccessRights(taskNumber, domainsNum, objectsNum);

        for (int i = 0; i < domainsNum; i++)
            userThreads[i] = new Thread(new User(i, i + 1, domainsNum, objectsNum, accessRights));

        for(int i = 0; i < domainsNum; i++)
            userThreads[i].start();
    }

    public static void main(String[] args) {
        handleCommandLineArguments(args);
    }
}
